(function(window, document, $, undefined) {
    'use strict';

    var Painel = {};

    Painel.deleteButton = function() {
        $('body').on('click', '.btn-delete', function(e) {
            e.preventDefault();
            var form = $(this).closest('form'),
                _this = this,
                message = ($(this).hasClass('btn-delete-multiple') ? 'Deseja excluir todos os registros?' :'Deseja excluir o registro?');

            bootbox.confirm({
                size: 'small',
                backdrop: true,
                message: message,
                buttons: {
                    'cancel': {
                        label: 'Cancelar',
                        className: 'btn-default btn-sm'
                    },
                    'confirm': {
                        label: '<span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir',
                        className: 'btn-primary btn-danger btn-sm'
                    }
                },
                callback: function(result) {
                    if (result) {
                        if ($(_this).hasClass('btn-delete-link')) return window.location.href = $(_this)[0].href;
                        form.submit();
                    }
                }
            });
        });
    };

    Painel.orderTable = function() {
        if (!$('.table-sortable').length) return;

        $('.table-sortable tbody').sortable({
            update: function () {
                var url   = $('base').attr('href') + '/painel/order',
                    data  = [],
                    table = $('.table-sortable').attr('data-table');

                $('.table-sortable tbody').children('tr').each(function(index, el) {
                    data.push(el.id)
                });

                $.post(url, { data: data, table: table });
            },
            handle: $('.btn-move')
        }).disableSelection();
    };

    Painel.datePicker = function() {
        if (!$('.datepicker').length) return;

        $('.datepicker').datepicker({
            closeText: 'Fechar',
            prevText: '&#x3c;Anterior',
            nextText: 'Pr&oacute;ximo&#x3e;',
            currentText: 'Hoje',
            monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
            'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
            'Jul','Ago','Set','Out','Nov','Dez'],
            dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
            dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
            dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 0,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        });

        if ($('.datepicker').val() == '') $('.datepicker').datepicker("setDate", new Date());
    };

    Painel.monthPicker = function() {
        if (!$('.monthpicker').length) return;

        $('.monthpicker').datepicker({
            changeMonth: true,
            changeYear: true,
            onClose: function() {
                var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                $(this).datepicker('setDate', new Date(year, month, 1));
            },
            beforeShow: function() {
                var selDate = $(this).val();
                if (selDate.length > 0) {
                    var year = selDate.substring(selDate.length - 4);
                    var month = selDate.substring(0, 2);
                    $(this).datepicker('option', 'defaultDate', new Date(year, month, 0))
                    $(this).datepicker('setDate', new Date(year, month, 0));
                }
            },
            monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
            'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
            'Jul','Ago','Set','Out','Nov','Dez'],
            dateFormat: 'mm/yy',
        });

        $('html > head').append('<style>.ui-datepicker-calendar { display: none; }.ui-datepicker select.ui-datepicker-month,.ui-datepicker select.ui-datepicker-year{ color: #2C3E50; font-weight: normal; }</style>');
        if ($('.monthpicker').val() == '') $('.monthpicker').datepicker("setDate", new Date());
    };

    Painel.textEditor = function() {
        if (!$('.ckeditor').length) return;

        CKEDITOR.config.language = 'pt-br';
        CKEDITOR.config.uiColor = '#dce4ec';

        var config = {
            padrao: {
                toolbar: [['Bold', 'Italic']]
            },

            clean: {
                toolbar: []
            }
        };

        $('.ckeditor').each(function (i, obj) {
            CKEDITOR.replace(obj.id, config[obj.dataset.editor]);
        });
    };

    Painel.imagesUpload = function() {
        var $wrapper = $('#images-upload');
        if (!$wrapper.length) return;

        var errors;

        $wrapper.fileupload({
            dataType: 'json',
            start: function(e) {
                if ($('.no-images').length) $('.no-images').fadeOut();

                if ($('.errors').length) {
                    errors = [];
                    $('.errors').fadeOut().html('');
                }
            },
            done: function (e, data) {
                $('#imagens').prepend($(data.result.body).hide().addClass('new'))
                             .sortable('refresh');
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);

                $('.progress-bar').css(
                    'width',
                    progress + '%'
                );
            },
            stop: function() {
                $('.progress-bar').css('width', 0);

                $('#imagens').find('.imagem').sort(function(a, b) {
                    var imgA = $(a).data('imagem').toUpperCase();
                    var imgB = $(b).data('imagem').toUpperCase();
                    var ordA = $(a).data('ordem');
                    var ordB = $(b).data('ordem');

                    if (ordA < ordB) return -1;
                    if (ordA > ordB) return 1;
                    if (imgA < imgB) return -1;
                    if (imgA > imgB) return 1;
                    return 0;
                }).appendTo($('#imagens'));

                $('#imagens .imagem.new').each(function(i) {
                    $(this).delay((i++) * 400).fadeIn(300);
                });

                $('.imagem').removeClass('new');

                if (errors.length) {
                    errors.forEach(function(message) {
                        $('.errors').append(message + '<br>');
                    });
                    $('.errors').fadeIn();
                }
            },
            fail: function(e, data) {
                var status       = data.jqXHR.status,
                    errorMessage = (status == '422' ? 'O arquivo deve ser uma imagem.' : 'Erro interno do servidor.'),
                    response     = 'Ocorreu um erro ao enviar o arquivo ' + data.files[0].name + ': ' + errorMessage;

                errors.push(response);
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    };

    Painel.orderImages = function() {
        var $wrapper = $('#imagens');
        if (!$wrapper.length) return;

        $wrapper.sortable({
            update: function () {
                var url   = $('base').attr('href') + '/painel/order',
                    data  = [],
                    table = $wrapper.attr('data-table');

                $wrapper.children('.imagem').each(function(index, el) {
                    el.dataset.ordem = index + 1;
                    $(el).data('ordem', index + 1);
                    data.push(el.id);
                });

                $.post(url, { data: data, table: table });
            },
            handle: '> img'
        }).disableSelection();
    };

    Painel.filtroSelect = function() {
        $('#filtro-select').on('change', function () {
            var id    = $(this).val(),
                base  = $('base').attr('href'),
                route = $(this).data('route');

            if (id) {
                window.location = base + '/' + route + '?filtro=' + id;
            } else {
                window.location = base + '/' + route;
            }
        });
    };

    Painel.maskMoney = function() {
        $('.maskmoney').maskMoney({
            thousands: '',
            decimal: ','
        });
    };

    Painel.init = function() {
        this.deleteButton();
        this.orderTable();
        this.datePicker();
        this.monthPicker();
        this.textEditor();
        this.imagesUpload();
        this.orderImages();
        this.filtroSelect();
        this.maskMoney();
    };

    $(document).ready(function() {
        Painel.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
