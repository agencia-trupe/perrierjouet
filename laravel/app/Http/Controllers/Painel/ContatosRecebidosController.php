<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ContatoRecebido;

class ContatosRecebidosController extends Controller
{
    public function index()
    {
        $contatosrecebidos = ContatoRecebido::orderBy('id', 'DESC')->paginate(15);

        return view('painel.contato.recebidos.index', compact('contatosrecebidos'));
    }

    public function show(ContatoRecebido $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.contato.recebidos.show', compact('contato'));
    }

    public function destroy(ContatoRecebido $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.pedidos.index')->with('success', 'Pedido excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }
}
