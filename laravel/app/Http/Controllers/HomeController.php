<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;
use App\Http\Controllers\Controller;

use App\Models\Produto;
use App\Models\ContatoRecebido;

class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.home');
    }

    public function produtos()
    {
        $produtos = Produto::ordenados()->get();

        return view('frontend.produtos', compact('produtos'));
    }

    public function pedido(Request $request)
    {
        if (!$request->get('produtos')) return back();

        return view('frontend.pedido')->with([
            'produtos' => $request->get('produtos')
        ]);
    }

    public function envio(ContatosRecebidosRequest $request)
    {
        $total  = 0;
        $pedido = [];

        foreach (json_decode($request->get('produtos')) as $id => $quantidade) {
            $produto = Produto::find($id);
            if ($produto) {
                $total += str_replace(',', '.', $produto->preco) * $quantidade;
                $pedido[] = $produto->nome . ' - R$ ' . $produto->preco . ' - ' . $quantidade . ' unidade' . ($quantidade > 1 ? 's' : '');
            }
        }

        $total = 'R$ ' . number_format($total, 2, ',', '');

        $input = $request->except('produtos');
        $input['pedido'] = join('<br>', $pedido) . '<br><br><strong>TOTAL DO PEDIDO: ' . $total . '</strong>';

        $pedido = ContatoRecebido::create($input);

        \Mail::send('emails.contato', ['pedido' => $pedido], function($message) use ($input)
        {
            $message->to($input['email'], $input['nome'])
                    ->subject('['.config('site.name').'] Recebemos seu pedido!')
                    ->replyTo('email@perrier.com', config('site.name'));
        });
        \Mail::send('emails.confirmacao', ['pedido' => $pedido], function($message) use ($input)
        {
            $message->to('email@perrier.com', config('site.name'))
                    ->subject('['.config('site.name').'] Novo Pedido')
                    ->replyTo($input['email'], $input['nome']);
        });

        return response()->json(['status' => 'success']);
    }
}
