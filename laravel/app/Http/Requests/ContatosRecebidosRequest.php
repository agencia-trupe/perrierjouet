<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatosRecebidosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cadastro'   => 'required',
            'sexo'       => 'required',
            'nome'       => 'required',
            'sobrenome'  => 'required',
            'email'      => 'required|email',
            'nascimento' => 'required',
            'telefone'   => 'required',
            'cpf_cnpj'   => 'required',
            'cep'        => 'required',
            'endereco'   => 'required',
            'numero'     => 'required',
            'bairro'     => 'required',
            'cidade'     => 'required',
            'estado'     => 'required',
            'pagamento'  => 'required',
            'produtos'   => 'required'
        ];
    }
}
