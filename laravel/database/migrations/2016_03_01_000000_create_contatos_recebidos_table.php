<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatosRecebidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contatos_recebidos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cadastro');
            $table->string('sexo');
            $table->string('nome');
            $table->string('sobrenome');
            $table->string('email');
            $table->string('nascimento');
            $table->string('telefone');
            $table->string('cpf_cnpj');
            $table->string('cep');
            $table->string('endereco');
            $table->string('numero');
            $table->string('complemento');
            $table->string('bairro');
            $table->string('cidade');
            $table->string('estado');
            $table->string('receber_noticias');
            $table->string('pagamento');
            $table->text('pedido');
            $table->boolean('lido')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contatos_recebidos');
    }
}
