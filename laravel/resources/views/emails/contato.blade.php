<!DOCTYPE html>
<html>
<head>
    <title>[{{ config('site.name') }}] Recebemos seu pedido!</title>
    <meta charset="utf-8">
</head>
<body>
    <p style="font-size:18px;font-family:'Times New Roman';">OBRIGADO. RECEBEMOS SEU PEDIDO REALIZADO EM {{ $pedido->created_at }}.</p>

    <p style="font-size:18px;font-family:'Times New Roman';">Para seu acompanhamento, seguem os dados do pedido:</p>

    <p style="font-size:18px;font-family:'Times New Roman';">{!! $pedido->pedido !!}</p>

    <p style="font-size:18px;font-family:'Times New Roman';">VOCÊ SERÁ CONTATADO EM ATÉ 72 HORAS PARA COMBINAR FORMA DE PAGAMENTO, DATA DE ENTREGA E CUSTOS DE FRETE.</p>
    <p style="font-weight:bold;font-size:26px;font-family:'Times New Roman';">BOAS FESTAS</p>
</body>
</html>

