<!DOCTYPE html>
<html>
<head>
    <title>[{{ config('site.name') }}] Novo Pedido</title>
    <meta charset="utf-8">
</head>
<body>
    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>Data:</strong>
        {{ $pedido->created_at }}
    </p>
    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>Cadastro:</strong>
        {{ $pedido->cadastro }}
    </p>
    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>Sexo:</strong>
        {{ $pedido->sexo }}
    </p>
    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>Nome:</strong>
        {{ $pedido->nome }}
    </p>
    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>Sobrenome:</strong>
        {{ $pedido->sobrenome }}
    </p>
    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>E-mail:</strong>
        {{ $pedido->email }}
    </p>
    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>Data de Nascimento:</strong>
        {{ $pedido->nascimento }}
    </p>
    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>Telefone/Celular:</strong>
        {{ $pedido->telefone }}
    </p>
    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>CPF ou CNPJ:</strong>
        {{ $pedido->cpf_cnpj }}
    </p>

    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>CEP:</strong>
        {{ $pedido->cep }}
    </p>
    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>Endereço:</strong>
        {{ $pedido->endereco }}
    </p>
    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>Número:</strong>
        {{ $pedido->numero }}
    </p>

    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>Complemento:</strong>
        {{ $pedido->complemento }}
    </p>
    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>Bairro:</strong>
        {{ $pedido->bairro }}
    </p>
    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>Cidade:</strong>
        {{ $pedido->cidade }}
    </p>
    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>Estado:</strong>
        {{ $pedido->estado }}
    </p>

    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>Aceita receber notícias e promoções:</strong>
        {{ $pedido->receber_noticias == 1 ? 'Sim' : 'Não' }}
    </p>

    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>Forma de Pagamento:</strong>
        {{ $pedido->pagamento }}
    </p>

    <p style="font-size:18px;font-family:'Times New Roman';">
        <strong>Pedido:</strong><br>
        {!! $pedido->pedido !!}
    </p>
</body>
</html>

