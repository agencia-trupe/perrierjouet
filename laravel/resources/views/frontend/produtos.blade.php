@extends('frontend.common.template')

@section('content')

    <script>
    var my_image = new Image();
    my_image.src = '{{ asset('assets/img/layout/garrafa-aberta.png') }}';
    </script>

    <div class="produtos">
        <div class="topo">
            <div class="garrafa">
                <div class="tampa"></div>
            </div>
        </div>

        <a href="#" class="scroll-down">SCROLL DOWN</a>

        <div class="produtos-lista">
            <div class="center">
                @foreach($produtos as $produto)
                <div class="produto">
                    <img src="{{ asset('assets/img/produtos/'.$produto->imagem) }}" alt="">
                    <p>{{ $produto->nome }}</p>
                    <p>R$ {{ $produto->preco }}</p>

                    <div class="contador" data-id="{{ $produto->id }}" data-quantidade="0" data-valor="{{ str_replace(',', '.', $produto->preco) }}">
                        <a href="#" class="menos"></a>
                        <span>0</span>
                        <a href="#" class="mais"></a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>

        <p class="qualidade">APRECIE NOSSA QUALIDADE COM MODERAÇÃO.</p>

        <div class="total">
            <form action="{{ route('pedido') }}" method="POST">
                {!! csrf_field() !!}
                <input type="hidden" name="produtos" id="produtos-form">
                <p>VALOR TOTAL <span id="valor-total"></span></p>
                <input type="submit" value="FINALIZAR PEDIDO">
            </form>
        </div>
    </div>

@endsection
