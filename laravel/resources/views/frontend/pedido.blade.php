@extends('frontend.common.template')

@section('content')

    <div class="pedido">
        <div class="pedido-center">
            <img src="{{ asset('assets/img/layout/marca-perrier-jouet.png') }}" alt="Champagne Perrier-Jouët">

            <form id="envio-pedido">
                <input type="hidden" name="produtos" id="produtos" value="{{ $produtos }}">
                <div class="radios">
                    <label>
                        <input type="radio" name="cadastro" value="Pessoa Física" required>
                        Pessoa Física
                    </label>
                    <label>
                        <input type="radio" name="cadastro" value="Pessoa Jurídica" required>
                        Pessoa Jurídica
                    </label>
                </div>
                <div class="radios">
                    <label>
                        <input type="radio" name="sexo" value="Feminino" required>
                        Mulher
                    </label>
                    <label>
                        <input type="radio" name="sexo" value="Masculino" required>
                        Homem
                    </label>
                </div>
                <div class="inputs">
                    <input type="text" name="nome" placeholder="NOME" required>
                    <input type="text" name="sobrenome" placeholder="SOBRENOME" required>
                    <input type="email" name="email" placeholder="E-MAIL" required>
                    <input type="nascimento" name="nascimento" placeholder="DATA DE NASCIMENTO" required maxlength="10">
                    <input type="text" name="telefone" placeholder="TELEFONE/CELULAR" required>
                    <input type="text" name="cpf_cnpj" placeholder="CPF OU CNPJ" required>
                </div>
                <div class="endereco">
                    <h3>ENDEREÇO DE ENTREGA</h3>
                    <input type="text" name="cep" id="cep" maxlength="9" size="10" placeholder="CEP" required>
                    <input type="text" name="endereco" id="endereco" placeholder="ENDEREÇO" required>
                    <input type="text" name="numero" id="numero" placeholder="NÚMERO" required>
                    <input type="text" name="complemento" id="complemento" placeholder="COMPLEMENTO">
                    <input type="text" name="bairro" id="bairro" placeholder="BAIRRO" required>
                    <input type="text" name="cidade" id="cidade" placeholder="CIDADE" required>
                    <input type="text" name="estado" id="estado" placeholder="UF" required>
                    <p>ESTE SITE REALIZA ENTREGAS APENAS NA CIDADE DE SÃO PAULO</p>
                </div>
                <div class="noticias">
                    <label>
                        <input type="checkbox" name="receber_noticias" value="1" checked>
                        ACEITO RECEBER NOTÍCIAS E PROMOÇÕES PERNOD RICARD
                    </label>
                </div>
                <div class="pagamento">
                    <p>OPÇÕES DE PAGAMENTO</p>
                    <label>
                        <input type="radio" name="pagamento" value="Cartão de Crédito" required>
                        Cartão de Crédito
                    </label>
                    <label>
                        <input type="radio" name="pagamento" value="Cartão de Débito" required>
                        Cartão de Débito
                    </label>
                    <label>
                        <input type="radio" name="pagamento" value="Transferência Bancária" required>
                        Transferência Bancária
                    </label>
                </div>
                <input type="submit" value="ENVIAR PEDIDO">
                <div class="aviso">
                    <p>APÓS REALIZADO O PEDIDO, VOCÊ TERÁ UM RETORNO EM ATÉ 72 HORAS PARA COMBINAR<br>FORMA DE PAGAMENTO, LOCAL, DATA DE ENTREGA E CUSTO DO FRETE.</p>
                    <h2>BOAS FESTAS</h2>
                </div>
            </form>
            <div class="sucesso">
                <p>Obrigado. Seu pedido foi enviado com sucesso.</p>
                <p>Você será contactado em até 72 horas para combinar forma de pagamento, data de entrega e custo de frete.</p>
                <h2>BOAS FESTAS</h2>
            </div>
        </div>

        <p class="qualidade">APRECIE NOSSA QUALIDADE COM MODERAÇÃO.</p>
    </div>

@endsection
