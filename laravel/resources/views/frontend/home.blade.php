@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="wrapper">
            <div class="middle">
                <p class="boas-festas">
                    BOAS FESTAS
                    <span>ESTE SITE REALIZA ENTREGAS<br>APENAS NA CIDADE DE SÃO PAULO.</span>
                </p>

                <img src="{{ asset('assets/img/layout/marca-perrier-jouet.png') }}" alt="Champagne Perrier-Jouët">

                <p class="pergunta">VOCÊ TEM MAIS DE 18 ANOS?</p>
                <div class="respostas">
                    <a href="#" id="resposta-nao">NÃO</a>
                    <a href="{{ route('produtos') }}">SIM</a>
                </div>
                <p class="aviso">
                    O CONSUMO E A VENDA DE ÁLCOOL<br>É PROIBIDO PARA MENORES DE 18 ANOS.
                </p>
            </div>
        </div>

        <p class="qualidade">APRECIE NOSSA QUALIDADE COM MODERAÇÃO.</p>
    </div>

@endsection
