<ul class="nav navbar-nav">
	<li @if(str_is('painel.produtos*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.produtos.index') }}">Produtos</a>
	</li>
    <li @if(str_is('painel.pedidos*', Route::currentRouteName())) class="active" @endif">
        <a href="{{ route('painel.pedidos.index') }}">
            Pedidos
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
        </a>
    </li>
</ul>
