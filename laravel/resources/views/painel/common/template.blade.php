<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url('/') }}">

    <title>{{ config('site.name') }} - Painel Administrativo</title>

    <link rel="stylesheet" href="{{ asset('assets/vendor/bootswatch-dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/jquery-ui/themes/cupertino/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/painel/painel.css') }}">
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">{{ config('site.name') }}</a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
                @include('painel.common.nav')

                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown @if(str_is('painel.usuarios*', Route::currentRouteName())) active @endif">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="glyphicon glyphicon-cog small" style="margin-right:3px"></span>
                            Sistema <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ route('painel.usuarios.index') }}">Usuários</a></li>
                            <li><a href="{{ route('logout') }}">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container" style="padding-bottom:30px;">
        @yield('content')
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{{ asset("assets/vendor/jquery/dist/jquery.min.js") }}"><\/script>')</script>
    <script src="{{ asset('assets/vendor/bootswatch-dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootbox.js/bootbox.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-maskmoney/dist/jquery.maskMoney.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/vendor/blueimp-file-upload/js/jquery.fileupload.js') }}"></script>
    <script src="{{ asset('assets/painel/painel.js') }}"></script>
</body>
</html>
