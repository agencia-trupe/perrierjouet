@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Contatos Recebidos</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $contato->created_at }}</div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Cadastro</label>
                <div class="well">{{ $contato->cadastro }}</div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Sexo</label>
                <div class="well">{{ $contato->sexo }}</div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Nome</label>
                <div class="well">{{ $contato->nome }}</div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Sobrenome</label>
                <div class="well">{{ $contato->sobrenome }}</div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>E-mail</label>
                <div class="well">{{ $contato->email }}</div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Data de Nascimento</label>
                <div class="well">{{ $contato->nascimento }}</div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Telefone/Celular</label>
                <div class="well">{{ $contato->telefone }}</div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>CPF ou CNPJ</label>
                <div class="well">{{ $contato->cpf_cnpj }}</div>
            </div>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label>CEP</label>
                <div class="well">{{ $contato->cep }}</div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Endereço</label>
                <div class="well">{{ $contato->endereco }}</div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Número</label>
                <div class="well">{{ $contato->numero }}</div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label>Complemento</label>
                <div class="well">{{ $contato->complemento }}</div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Bairro</label>
                <div class="well">{{ $contato->bairro }}</div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Cidade</label>
                <div class="well">{{ $contato->cidade }}</div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Estado</label>
                <div class="well">{{ $contato->estado }}</div>
            </div>
        </div>
    </div>

    <hr>

    <div class="form-group">
        <label>Aceita receber notícias e promoções</label>
        <div class="well">{{ $contato->receber_noticias == 1 ? 'Sim' : 'Não' }}</div>
    </div>

    <hr>

    <div class="form-group">
        <label>Forma de Pagamento</label>
        <div class="well">{{ $contato->pagamento }}</div>
    </div>

    <hr>

    <div class="form-group">
        <label>Pedido</label>
        <div class="well">{!! $contato->pedido !!}</div>
    </div>

    <a href="{{ route('painel.pedidos.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
