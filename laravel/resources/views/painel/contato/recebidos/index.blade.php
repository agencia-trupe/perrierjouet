@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>Pedidos</h2>
    </legend>

    @if(!count($contatosrecebidos))
    <div class="alert alert-warning" role="alert">Nenhum pedido recebido.</div>
    @else
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Data</th>
                <th>Nome</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($contatosrecebidos as $contato)

            <tr class="tr-row @if(!$contato->lido) warning @endif">
                <td>{{ $contato->created_at }}</td>
                <td>{{ $contato->nome }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.pedidos.destroy', $contato->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.pedidos.show', $contato->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Ler mensagem
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    {!! $contatosrecebidos->render() !!}
    @endif

@stop
