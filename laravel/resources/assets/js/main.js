(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.menorDeIdade = function() {
        $('#resposta-nao').click(function(event) {
            event.preventDefault();

            var $elementos = $('.boas-festas, .pergunta, .respostas');

            $elementos.fadeTo(500, 0, function(){
               $elementos.css('visibility', 'hidden');
            });

            $('.aviso').addClass('destaque');
        });
    };

    App.animacaoGarrafa = function() {
        var $garrafa = $('.garrafa');

        $(window).scroll(function() {
            if ($(this).scrollTop() >= 1) {
                $garrafa.addClass('aberta').parent().addClass('sobe');
            } else {
                $garrafa.removeClass('aberta').parent().removeClass('sobe');
            }
        });
    };

    App.scrollDown = function() {
        var $botao = $('.scroll-down'),
            $lista = $('.produtos-lista');

        $botao.click(function(e) {
            e.preventDefault();

            $('html, body').animate({
                scrollTop: 1
            }, 1);
            setTimeout(function() {
                $('html, body').animate({
                    scrollTop: $lista.offset().top - 25
                }, 2500);
            }, 500);
        });
    };

    App.contadores = function() {
        $('.mais').click(function(event) {
            event.preventDefault();

            var $contador = $(this).parent();

            $contador.data('quantidade', $contador.data('quantidade') + 1);
            $contador.find('span').text($contador.data('quantidade'));

            atualizaTotal();
            atualizaForm();
        });

        $('.menos').click(function(event) {
            event.preventDefault();

            var $contador = $(this).parent();

            if ($contador.data('quantidade') === 0) return;

            $contador.data('quantidade', $contador.data('quantidade') - 1);
            $contador.find('span').text($contador.data('quantidade'));

            atualizaTotal();
            atualizaForm();
        });

        var atualizaTotal = function() {
            var total = 0;

            $('.contador').each(function(index, element) {
                var valor = $(element).data('quantidade') * $(element).data('valor');
                total += valor;
            });

            if (total > 0) {
                $('#valor-total').text('R$ ' + total.toFixed(2).replace('.', ','));
                $('.total').show();
                $('.produtos').addClass('com-total');
            } else {
                $('.total').hide();
                $('.produtos').removeClass('com-total');
            }
        };

        var atualizaForm = function() {
            var produtos = {};

            $('.contador').each(function(index, element) {
                if ($(element).data('quantidade') > 0) {
                    produtos[$(element).data('id')] = $(element).data('quantidade');
                }
            });

            $('#produtos-form').val(JSON.stringify(produtos));
        };
    };

    App.CEP = function() {
        function limpa_formulário_cep() {
            $("#endereco").val("");
            $("#bairro").val("");
            $("#cidade").val("");
            $("#estado").val("");
        }

        $("#cep").blur(function() {
            var cep = $(this).val().replace(/\D/g, '');

            if (cep !== "") {
                var validacep = /^[0-9]{8}$/;

                if (validacep.test(cep)) {
                    $("#endereco").val("...");
                    $("#bairro").val("...");
                    $("#cidade").val("...");
                    $("#estado").val("...");

                    $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
                        if (!("erro" in dados)) {
                            $("#endereco").val(dados.logradouro);
                            $("#bairro").val(dados.bairro);
                            $("#cidade").val(dados.localidade);
                            $("#estado").val(dados.uf);
                        } else {
                            limpa_formulário_cep();
                            alert("CEP não encontrado.");
                        }
                    });
                } else {
                    limpa_formulário_cep();
                    alert("Formato de CEP inválido.");
                }
            } else {
                limpa_formulário_cep();
            }
        });
    };

    App.envioPedido = function() {
        $('#envio-pedido').on('submit', function(event) {
            event.preventDefault();
            var $form = $(this);

            $.ajax({
                type: "POST",
                url: $('base').attr('href') + '/pedido/envio',
                data: {
                    cadastro: $('input[name=cadastro]:checked').val(),
                    sexo: $('input[name=sexo]:checked').val(),
                    nome: $('input[name=nome]').val(),
                    sobrenome: $('input[name=sobrenome]').val(),
                    email: $('input[name=email]').val(),
                    nascimento: $('input[name=nascimento]').val(),
                    telefone: $('input[name=telefone]').val(),
                    cpf_cnpj: $('input[name=cpf_cnpj]').val(),
                    cep: $('input[name=cep]').val(),
                    endereco: $('input[name=endereco]').val(),
                    numero: $('input[name=numero]').val(),
                    complemento: $('input[name=complemento]').val(),
                    bairro: $('input[name=bairro]').val(),
                    cidade: $('input[name=cidade]').val(),
                    estado: $('input[name=estado]').val(),
                    receber_noticias: $('input[name=receber_noticias]').is(':checked') ? 1 : 0,
                    pagamento: $('input[name=pagamento]:checked').val(),
                    produtos: $('input[name=produtos]').val(),
                },
                success: function(data) {
                    $form.fadeOut('fast', function() {
                        $('.sucesso').fadeIn();
                    });
                },
                error: function(data) {
                    var mensagens = JSON.parse(data.responseText), erro = '';

                    for (var field in mensagens) {
                        erro += mensagens[field][0] + "\n";
                    }

                    alert(erro);
                },
                dataType: 'json'
            });
        });
    };

    App.init = function() {
        this.menorDeIdade();
        this.animacaoGarrafa();
        this.scrollDown();
        this.contadores();
        this.CEP();
        this.envioPedido();
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
